---
marp: true
paginate: true
footer: Хорошая архитектура дизайнера
---

<!--
backgroundColor: orange
 -->
# Хорошая архитектура дизайнера

На примере дизайнера ответов ActiveReports WebDesigner

---
<!-- backgroundColor: white -->

# Обзор требований

* представление WYSIWYG
* визуальное редактирование
  * действия  мышью
  * drag-n-drop
  * сетка, рулеры, прилипание
  * обратная реакция, предпросмотр
* отмена
* множественные операции
* реактивность

> это все разложить на много страниц

---

# Обзор VS Designer Model

* Component
* Site -> Designer
* DesignerHost
* ServiceContainer
* IDesignerView
* ICommandService
* Notification Service
* Undo Manager
* ...

Slide text goes here

---

# MVU

> Picture goes here

* модель
* представление
* действия (redux)
* редюсеры (immutable)

---

# Дизайнер (обзор)

* designer shell / Core UI
* модель всего дизайнера (отдельный слайд)
* "контекст" / общие данные
* диспетчеризация

---

# Идеи для рассказа

* Event sourcing
* Что можно улучшить

