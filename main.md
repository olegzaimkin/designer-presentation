---
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
marp: true
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
---

![bg left:40% 80%](https://upload.wikimedia.org/wikipedia/commons/7/73/ActiveReports-Logo.png)

# **AR Web Designer**

Обзор архитектуры и дизайна

---
<!-- backgroundColor: white -->

# Почему дизайнер сложный

* представление WYSIWYG
* визуальное редактирование
  * действия  мышью
  * drag-n-drop
  * сетка, рулеры, прилипание
  * обратная реакция, предпросмотр
* отмена
* множественные операции
* реактивность

---

# План доклада

* React/Redux/MVU
* Иммутабельная модель
* Представление как чистая функция
* Как работают обновления
* Как устроен reducer

* Как собрать все вместе
* что такое designer shell (core-ui)?

---

# План доклада (продолжение)

* как устроено редактирование свойств
* а где транзации и Undo?
* как работает перетаскивание и операции с мышью
* дефолтные значения, это же сложно...
* контейнеры и множественный выбор
* ...
* сколько длится пересборка проекта?
* какое отношение _это_ имеет к WinForms дизайнеру

---

# MVU

![bg width:800px right:70%](mvu-unidir-ui-arch.jpg)

* модель

---

# Модель

> здесь представлена модель приложения в целом
> TODO: положить образец

```javascript
type Report = {
	name: string
	sections: ReportSection[]
}
type ReportSection = HeaderFooterSection | DetailSection
```

---

# MVU

![bg width:800px right:70%](mvu-unidir-ui-arch.jpg)

* модель
* view

---

# Представление

```javascript
const reportView = ({ name, sections }) =>
	(<div className="report-view">
		{ sections.map((section,i) => <SectionView key={i} item={section}/>)}
	</div>);
```

---

# MVU

![bg width:800px right:70%](mvu-unidir-ui-arch.jpg)

* модель
* view
* actions

---

# Команды

```javascript
const openDialogView = ({ fileName, dispatch }) =>
	(<button onClick={dispatch(actions.openFile(fileName))}>
	</div>);
```

---

# MVU

![bg width:800px right:70%](mvu-unidir-ui-arch.jpg)

* модель
* view
* actions
* reducers

---

# Reducer

```javascript
function update(state: Model, action: Msg): Model = {
  switch (action.type) {
	  case "RPT/SELECT":
		  const { selection } = action;
		  return update(state, { selection: { $set: selection }});
  }
}
```

---

# Immutability helpers

```javascript
import update from 'react-addons-update';

const newData = update(myData, {
  x: {y: {z: {$set: 7}}},
  a: {b: {$push: [9]}}
});
```

---

# Available commands

* `{$push: array}` push() all the items in array on the target.
* `{$unshift: array}` unshift() all the items in array on the target.
* `{$splice: array of arrays}` for each item in arrays call splice() on the * target with the parameters provided by the item.
* `{$set: any}` replace the target entirely.
* `{$merge: object}` merge the keys of object with the target.
* `{$apply: function}` passes in the current value to the function and updates it with the new returned value.

---

# Собираем все вместе

* designer shell / Core UI
* designer application
* redux store
* dispatch

---

# core-ui 3.x

![bg width:800px right:70%](core-ui.png)

* menu
* toolbar
* surface
* sidebar
* properties
* statusbar
* ...

---

# redux

```javascript
' TODO показать маркап стора, рассказать про контекст, пример использованя dispatch
```

---

<!--
theme: enable-all-auto-scaling
auto-scaling: true
-->

# Про что интересно послушать?

* панель свойств (как устроена и как задать свойства)
* где транзации и Undo?
* как работает перетаскивание и операции с мышью
* дефолтные значения, это же сложно...
* контейнеры, множественный выбор и перетаскивание
* сколько длится пересборка проекта? (ответ: 5 сек)
* как сделан FPL режим
* ... свой вопрос

---

# Properties

* описание
* множественный выбор
* вложенные объекты
* упрощенный вид
* кастомные редакторы

---

# Undo

* запоминаем все предыдущие состояния
* восстановление сеанса

---

# Drag-n-drop

* react-dragdrop

---

# Составной элемент (контейнер)

* позиционирование "детей"
* "выделение"
* "бросание" элемента
* диспетчеризация событий (?)

---

# Что получилось (итоги)

* качество
* скорость разработки
* скорость работы
* простота
* кривая обучения

